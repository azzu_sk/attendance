//
//  AttendanceTableViewCell.swift
//  AttendanceApp
//
//  Created by Nan on 08/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class AttendanceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblInTime : UILabel!
    @IBOutlet weak var lblOutTime : UILabel!
    
   override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(strDate: String, strInTime: String, strOutTime: String ){
        lblDate.text = strDate
        lblInTime.text = strInTime
        lblOutTime.text = strOutTime
    }

}
