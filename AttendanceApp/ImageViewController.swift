//
//  ImageViewController.swift
//  AttendanceApp
//
//  Created by Nan on 28/12/18.
//  Copyright © 2018 Nan. All rights reserved.
//

import UIKit
import AssetsLibrary
import Alamofire

class ImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imgView: UIImageView!
    var imgSendData : Data!
    var filee = ""
    var logId = 13
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func action(_ sender: Any) {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self
        myPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPicController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let img_data = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        let img_pth = img_data.cgImage
        let imageData : Data = img_data.pngData()!
        if let imgstrPth = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL{
            ALAssetsLibrary().asset(for: imgstrPth as URL, resultBlock: { (asset) in
                let fileName = asset?.defaultRepresentation()?.filename()
                print(fileName)
                self.imgSendData = img_data.jpegData(compressionQuality: 0.2)
                self.filee = fileName!
                self.actionInsertData()
            }, failureBlock: nil)
        }
        //let img_str = imageData.base64EncodedData()
        //print(img_str)
        //print(img_pth)
        //print("desc",img_data)
        imgView.image = UIImage(cgImage: img_pth!)
        self.dismiss(animated: true, completion: nil)
//        actionInsertData()
    }
    
    func actionInsertData() {
        print("insertData")
        var sendLogid =  String(logId)
        let param = ["logid": sendLogid]
        //var image = UIImage.init(named: "404.png")
        //imgSendData = image?.jpegData(compressionQuality: 0.2)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(self.imgSendData, withName: "fileToUpload", fileName: "name1.png", mimeType: "image/png")
            for (key, val) in param {
                multipartFormData.append((val).data(using: String.Encoding.utf8)!, withName: key, mimeType: "text/plain")
            
                    }
        }, to: "http://192.168.64.2/attendance/uploadData.php?") { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.uploadProgress(closure:{ (progress) in
                    print("Upload progress \(progress.fractionCompleted)")
                    self.logId += 1
                })
                upload.responseString(completionHandler: { (responce) in
                    print(responce)
                })
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
}
