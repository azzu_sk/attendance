//
//  MainViewController.swift
//  AttendanceApp
//
//  Created by Nan on 09/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
//import SwipeMenuViewController

class MainViewController: UIViewController {

    @IBOutlet weak var menuAction: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        menuAction.addTarget(self.revealViewController(), action: "revealToggle", for: UIControl.Event.touchUpInside)
         menuAction.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControl.Event.touchUpInside)
        
        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
    }
}
