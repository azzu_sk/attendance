//
//  OfficialCell.swift
//  AttendanceApp
//
//  Created by Nan on 11/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class OfficialCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblTitleContent:UILabel!
    @IBOutlet weak var editBtn:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
