//
//  OfficialViewController.swift
//  AttendanceApp
//
//  Created by Nan on 11/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Alamofire

@available(iOS 11.0, *)
class OfficialViewController: UIViewController {
    
    @IBOutlet weak var tblView:UITableView!
    
//        var getOfficialUrl = "http://localhost:8080/attendance/EmpDetail/getDetails.php"
//        var updateOfficialUrl = "http://localhost:8080/attendance/EmpDetail/updateDetails.php"
    var getOfficialUrl = "https://testattendancerox.000webhostapp.com/attendance/EmpDetail/getDetails.php"
    var updateOfficialUrl = "https://testattendancerox.000webhostapp.com/attendance/EmpDetail/updateDetails.php"
    var emp_Details:Array<Any>!
    var empArr = [String]()
    var index = 0
    var officialTitle = ["Emp_Code","Name","Role","Email","Department","Unit","Position","Status"]
    var updateKey = ["emp_code","emp_name","emp_role","emp_email","emp_department","emp_unit","emp_position","emp_status"]
    
    var appDel:AppDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        appDel = UIApplication.shared.delegate as! AppDelegate
        getOfficial()
    }
    @IBAction func actionHome(_ sender: Any) {
//        appDel.getDataCntr += 1
        let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController")
        self.present(navigationController, animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    
}
@available(iOS 11.0, *)
extension OfficialViewController:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return officialTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "OfficialCell") as! OfficialCell
        cell.lblTitle.text = officialTitle[indexPath.row]
        if (empArr.count != 0) {
            cell.lblTitleContent.text = " : \(empArr[indexPath.row])"
        }
        else{
            cell.lblTitleContent.text = " : No Data"
        }
        //        print(empArr.count)
        //        print(empArr[indexPath.row])
        //        cell.lblTitleContent.text = " : \(empArr[indexPath.row])"
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(getAlert), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        self.tblView.reloadData()
    }
    func getOfficial() {
        var official_tb = "official_tb"
        Alamofire.upload(multipartFormData: { (multipartform) in
            multipartform.append(official_tb.data(using: String.Encoding.utf8)!, withName: "table")
        }, to: getOfficialUrl) { (reponce) in
            switch reponce{
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (jsonRess) in
                    //                    print(jsonRess)
                    if let data = jsonRess.result.value as? NSDictionary{
                        if let emp_data = data["data"] as? NSDictionary{
                            //                            print(emp_data)
                            if let emp_data_arr = emp_data["emp"] as? NSArray{
                                //                                print(emp_data_arr)
                                for emp_value in emp_data_arr{
                                    let jsonValue = emp_value as! NSDictionary
                                    //                                    print(jsonValue.value(forKey: "p_id"))
                                    //                                    self.emp_Details = jsonValue.allValues
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_code") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_name") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_role") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_email") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_department") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_unit") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_position") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_status") as! String)
                                    //                                    print(self.empArr)
                                }
                                //                                print(self.emp_Details)
                            }
                        }
                    }
                    self.tblView.reloadData()
                })
            case .failure(_):
                print(EncodingError.self)
                break
            }
        }
    }
    @objc func getAlert(sender:UIButton) {
        //        print(sender.tag)
        self.index = sender.tag
        let altController = UIAlertController(title: "Update", message: "Insert Value", preferredStyle: .alert)
        
        let altAction = UIAlertAction(title: "Update", style: .default) { (updateAction) in
            let txtField = altController.textFields![0] as UITextField
            
            self.empArr.remove(at: self.index)
            var str = txtField.text as! String
            var key = self.updateKey[self.index]
            self.empArr.insert(str, at: self.index)
            //            print(str)
            //            print(key)
            self.updateOfficial(text: str, field: key)
            self.tblView.reloadData()
        }
        altController.addTextField { (txtField) in
            txtField.placeholder = "Enter Value"
        }
        
        let clrAction = UIAlertAction(title: "Cancle", style: .destructive, handler: nil)
        
        altController.addAction(clrAction)
        altController.addAction(altAction)
        self.present(altController, animated: true, completion: nil)
    }
    func updateOfficial(text: String, field: String) {
        var param = [
            "table":"official_tb",
            "field":field,
            "update":text
        ]
        Alamofire.upload(multipartFormData: { (multipartform) in
            for (key,value) in param{
                multipartform.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: updateOfficialUrl) { (reponce) in
            switch reponce{
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (jsonRess) in
                    //                    print(jsonRess)
                    self.tblView.reloadData()
                })
            case .failure(_):
                print(EncodingError.self)
                break
            }
        }
    }
    
}
