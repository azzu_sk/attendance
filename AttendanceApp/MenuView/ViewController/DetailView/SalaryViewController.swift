//
//  SalaryViewController.swift
//  AttendanceApp
//
//  Created by Nan on 11/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Alamofire

class SalaryViewController: UIViewController {
    
    @IBOutlet weak var tblView:UITableView!
    
    
//        var getSalaryUrl = "http://localhost:8080/attendance/EmpDetail/getDetails.php"
//        var updateSalaryUrl = "http://localhost:8080/attendance/EmpDetail/updateDetails.php"
    var getSalaryUrl = "https://testattendancerox.000webhostapp.com/attendance/EmpDetail/getDetails.php"
    var updateSalaryUrl = "https://testattendancerox.000webhostapp.com/attendance/EmpDetail/updateDetails.php"
    var emp_Details:Array<Any>!
    var empArr = [String]()
    var index = 0
    var salaryTitle = ["Currency","Salary","A/C_Type","A/C_No","Bank_Name"]
    var updateKey = ["emp_currency","emp_salary","emp_ac_type","emp_ac_num","emp_bank_name"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getSalary()
    }
    @IBAction func actionHome(_ sender: Any) {
        let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController")
        self.present(navigationController, animated: true, completion: nil)
    }
}
extension SalaryViewController:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return salaryTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "SalaryCell") as! SalaryCell
        cell.lblTitle.text = salaryTitle[indexPath.row]
        if (empArr.count != 0) {
            cell.lblTitleContent.text = " : \(empArr[indexPath.row])"
        }
        else{
            cell.lblTitleContent.text = " : No Data"
        }
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(getAlert), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        self.tblView.reloadData()
    }
    func getSalary() {
        var salary_tb = "salary_tb"
        Alamofire.upload(multipartFormData: { (multipartform) in
            multipartform.append(salary_tb.data(using: String.Encoding.utf8)!, withName: "table")
        }, to: getSalaryUrl) { (reponce) in
            switch reponce{
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (jsonRess) in
                    print(jsonRess)
                    if let data = jsonRess.result.value as? NSDictionary{
                        if let emp_data = data["data"] as? NSDictionary{
                            print(emp_data)
                            if let emp_data_arr = emp_data["emp"] as? NSArray{
                                print(emp_data_arr)
                                for emp_value in emp_data_arr{
                                    let jsonValue = emp_value as! NSDictionary
                                    print(jsonValue.value(forKey: "p_id"))
                                    //                                    self.emp_Details = jsonValue.allValues
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_code") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_currency") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_salary") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_ac_type") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_ac_num") as! String)
                                    
                                    self.empArr.append(jsonValue.value(forKey: "emp_bank_name") as! String)
                                    print(self.empArr)
                                }
                                //                                print(self.emp_Details)
                            }
                        }
                    }
                    self.tblView.reloadData()
                })
            case .failure(_):
                print(EncodingError.self)
                break
            }
        }
    }
    @objc func getAlert(sender:UIButton) {
        print(sender.tag)
        self.index = sender.tag
        let altController = UIAlertController(title: "Update", message: "Insert Value", preferredStyle: .alert)
        
        let altAction = UIAlertAction(title: "Update", style: .default) { (updateAction) in
            let txtField = altController.textFields![0] as UITextField
            
            self.empArr.remove(at: self.index)
            var str = txtField.text as! String
            var key = self.updateKey[self.index]
            self.empArr.insert(str, at: self.index)
            print(str)
            print(key)
            self.updateOfficial(text: str, field: key)
            self.tblView.reloadData()
        }
        altController.addTextField { (txtField) in
            txtField.placeholder = "Enter Value"
        }
        
        let clrAction = UIAlertAction(title: "Cancle", style: .destructive, handler: nil)
        
        altController.addAction(clrAction)
        altController.addAction(altAction)
        self.present(altController, animated: true, completion: nil)
    }
    func updateOfficial(text: String, field: String) {
        var param = [
            "table":"salary_tb",
            "field":field,
            "update":text
        ]
        Alamofire.upload(multipartFormData: { (multipartform) in
            for (key,value) in param{
                multipartform.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: updateSalaryUrl) { (reponce) in
            switch reponce{
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (jsonRess) in
                    print(jsonRess)
                    self.tblView.reloadData()
                })
            case .failure(_):
                print(EncodingError.self)
                break
            }
        }
    }
    
}
