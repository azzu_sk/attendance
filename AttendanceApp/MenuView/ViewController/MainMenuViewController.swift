//
//  MainMenuViewController.swift
//  AttendanceApp
//
//  Created by Nan on 11/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Alamofire

class MainMenuViewController: UIViewController{

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    
    var strName = ""
    var strImage = ""
    var strImgPth = "https://testattendancerox.000webhostapp.com/attendance/uploads/"
    var getImageUrl = "https://testattendancerox.000webhostapp.com/attendance/getData.php"
//    var strImgPth = "/Users/nan/.bitnami/stackman/machines/xampp/volumes/root/htdocs/attendance/uploads/"
//    var getImageUrl = "http://localhost:8080/attendance/getData.php"
    var strLogid = ""
    var strEmail = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        strLogid = UserDefaults.standard.string(forKey: "logid")!
        strEmail = UserDefaults.standard.string(forKey: "email")!
        getImage(strId: strLogid, strPswd: strEmail)
        profileImg.layer.borderWidth = 1
        profileImg.layer.masksToBounds = false
        profileImg.layer.cornerRadius = profileImg.frame.height / 2
        profileImg.clipsToBounds = true
        lblUserName.text = UserDefaults.standard.string(forKey: "name")
        
        
    }
    
    func getImage(strId: String, strPswd: String) {
        var chkId = ""
        var chkPswd = ""
        Alamofire.request(getImageUrl).responseJSON { (responce) in
            print(responce)
            let resData = responce.result.value as! NSDictionary
            if let reqData : NSArray = resData["user"] as! NSArray{
                for loopVal in  reqData{
                    let jsonData = loopVal as! NSDictionary
                    chkId = jsonData.value(forKey: "log_id") as! String
                    chkPswd = jsonData.value(forKey: "user_email") as! String
                    if(strId == chkId && strPswd == chkPswd )
                    {
                        self.strName = jsonData.value(forKey: "user_name") as! String
                        self.strImage = jsonData.value(forKey: "user_img") as! String
                    }
                    
                }
                
                var imgStr = self.strImgPth + self.strImage
//                let imgUrl = URL(string: imgStr)
//                self.profileImg.image = UIImage(named: imgStr)
                let imgUrl = URL(string: imgStr)
                if(imgUrl == nil){
                    self.profileImg.image = UIImage(named: "404")
                }
                else{
                    let imgData = try? Data(contentsOf: imgUrl!)
                    self.profileImg.image = UIImage(data: imgData!)
                }
                
            }
        }
    }

}
