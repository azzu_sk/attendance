//
//  MenuBtn.swift
//  AttendanceApp
//
//  Created by Nan on 11/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
//OfficialViewController
class MenuBtn: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func actionOfficial(_ sender: Any) {
        let navControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OfficialViewController")
        self.present(navControl, animated: true, completion: nil)
        
    }
    
    @IBAction func actionSalary(_ sender: Any) {
        let navControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SalaryViewController")
        self.present(navControl, animated: true, completion: nil)
    }
    
    @IBAction func actionPersonal(_ sender: Any) {
        let navControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PersonalViewController")
        self.present(navControl, animated: true, completion: nil)
    }
    
    @IBAction func actionContact(_ sender: Any) {
        let navControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactViewController")
        self.present(navControl, animated: true, completion: nil)
        
    }
    
    @IBAction func actionSkill(_ sender: Any) {
        let navControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SkillViewController")
        self.present(navControl, animated: true, completion: nil)
        
    }
    
    @IBAction func actionExperiance(_ sender: Any) {
        let navControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ExperianceViewController")
        self.present(navControl, animated: true, completion: nil)
        
    }
    
    
    
}
