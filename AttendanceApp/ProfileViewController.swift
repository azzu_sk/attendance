//
//  ProfileViewController.swift
//  AttendanceApp
//
//  Created by Nan on 31/12/18.
//  Copyright © 2018 Nan. All rights reserved.
//

import UIKit
import QuartzCore
import Alamofire
import UserNotifications

@available(iOS 11.0, *)
class ProfileViewController: UIViewController {
    
    
    @IBOutlet weak var activityProgress: UIActivityIndicatorView!
    @IBOutlet weak var setName : UILabel!
    @IBOutlet weak var setLogid : UILabel!
    @IBOutlet weak var setEmail : UILabel!
    @IBOutlet weak var setAddress : UILabel!
    @IBOutlet weak var setImage : UIImageView!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var attendanceBtn: UIButton!
    @IBOutlet weak var actionMenu: UIBarButtonItem!
    
    var getDataCntr = 0
    var appDel:AppDelegate!
    var setOtp = ""
    var strLogid = ""
    var strEmail = ""
    var strName = ""
    var strAddress = ""
    var chkDevice = ""
    var strImage = ""
    var strContact = ""
    var strImagePth = "https://testattendancerox.000webhostapp.com/attendance/uploads/"
    var getDataUrl = "https://testattendancerox.000webhostapp.com/attendance/getData.php"
    var genarateOTPUrl = "https://testattendancerox.000webhostapp.com/attendance/generateOTP.php"
    var upadteOTPUrl = "https://testattendancerox.000webhostapp.com/attendance/updateOtp.php"
    
    //        var strImagePth = "/Users/nan/.bitnami/stackman/machines/xampp/volumes/root/htdocs/attendance/uploads/"
    //        var getDataUrl = "http://localhost:8080/attendance/getData.php"
    //        var genarateOTPUrl = "http://localhost:8080/attendance/generateOTP.php"
    //        var upadteOTPUrl = "http://localhost:8080/attendance/updateOtp.php"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        activityProgress.startAnimating()
        activityProgress.isHidden = false
        
        appDel = UIApplication.shared.delegate as! AppDelegate
        
        setImage.layer.borderWidth = 1
        setImage.layer.masksToBounds = false
        setImage.layer.cornerRadius = setImage.frame.height / 2
        setImage.clipsToBounds = true
        
        attendanceBtn.layer.cornerRadius = 10
        //attendanceBtn.clipsToBounds = true
        
        actionBtn.layer.cornerRadius = 10
        actionBtn.clipsToBounds = true
        
        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        
        
        
        setValues()
    }
    @IBAction func actionMenufunc(_ sender: Any) {
        revealViewController()?.revealToggle(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(appDel.btncntr == "in"){
            attendanceBtn.backgroundColor = #colorLiteral(red: 0, green: 0.5788033837, blue: 0.01762887103, alpha: 1)
            attendanceBtn.setTitle("IN", for: .normal)
        }
        else{
            attendanceBtn.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            attendanceBtn.setTitle("OUT", for: .normal)
        }
        if(appDel.promt == 1)
        {
            let alert = UIAlertController(title: "Logged In", message: "Welcome", preferredStyle: .alert)
            let defAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(defAction)
            self.present(alert, animated: true, completion: nil)
            appDel.promt = 0
        }
        if(appDel.promt == 0)
        {
            let alert = UIAlertController(title: "Welcome", message: UserDefaults.standard.string(forKey: "name"), preferredStyle: .alert)
            let defAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(defAction)
            self.present(alert, animated: true, completion: nil)
            appDel.promt = 2
        }
        
        strLogid = UserDefaults.standard.string(forKey: "logid")!
        strEmail = UserDefaults.standard.string(forKey: "email")!
        //        print(strLogid)
        //        print(strEmail)
        getDataCntr = appDel.getDataCntr
//        print(getDataCntr)
        if(getDataCntr % 2 == 0){
            activ()
            getData(strId: strLogid, strPswd: strEmail)
        }
        else{
            stop()
            appDel.getDataCntr -= 1
        }
        //getData(strId: strLogid, strPswd: strEmail)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //        print("DidAppear")
        
        //        var dateNtime = getDate()
        //        var splitDataNTime = dateNtime.split(separator: ".")
        //        var altDate = String(splitDataNTime[0])
        //        var altTime = String(splitDataNTime[1])
        //        var getdate = altDate.split(separator: "-")
        //
        //        sysDate = Int(getdate[0])!
        
        //        print(appDel.btncntr)
        //        if(appDel.btncntr % 2 == 0){
        //            attendanceBtn.setTitle("IN", for: .normal)
        //        }
        //        else{
        //            attendanceBtn.setTitle("OUT", for: .normal)
        //        }
        //        if(appDel.promt == 1)
        //        {
        //            let alert = UIAlertController(title: "Logged In", message: "Welcome", preferredStyle: .alert)
        //            let defAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        //            alert.addAction(defAction)
        //            self.present(alert, animated: true, completion: nil)
        //            appDel.promt = 0
        //        }
        //        if(appDel.promt == 0)
        //        {
        //            let alert = UIAlertController(title: "Welcome", message: UserDefaults.standard.string(forKey: "name"), preferredStyle: .alert)
        //            let defAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        //            alert.addAction(defAction)
        //            self.present(alert, animated: true, completion: nil)
        //            appDel.promt = 2
        //        }
        //
        //        strLogid = UserDefaults.standard.string(forKey: "logid")!
        //        strEmail = UserDefaults.standard.string(forKey: "email")!
        ////        print(strLogid)
        ////        print(strEmail)
        //        getData(strId: strLogid, strPswd: strEmail)
        //        print("did")
        //        setValues()
        //        if(btnCntr == 0){
        //            setBtnLbl.titleLabel?.text = "changed"
        //        }
        //         var helloWorldTimer = Timer.scheduledTimer(timeInterval: 7200.0, target: self, selector: #selector(ProfileViewController.sayHello), userInfo: nil, repeats: true)
    }
    @IBAction func actionUpdate(_ sender: Any) {
        appDel.getDataCntr += 1
        self.performSegue(withIdentifier: "UpdateViewController", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "UpdateViewController"){
            let dest  = segue.destination as! UpdateViewController
            
            dest.strName = self.strName
            dest.strContact = self.strContact
            dest.strEmail = self.strEmail
            dest.strAddress = self.strAddress
            dest.strImage = self.strImage
        }
        
        if(segue.identifier == "fillAttedance"){
            let newDest = segue.destination as! UsrDataViewController
            newDest.strOtp = self.setOtp
            if(appDel.btncntr == "in"){
                newDest.sendChkOut = "1"
            }
            else{
                newDest.sendChkOut = "0"
            }
            
        }
    }
    
    func setValues() {
        //        activ()
        setName.text = ": " + strName
        setLogid.text = ": " + strLogid
        setEmail.text = ": " + strEmail
        setAddress.text = ": " + strAddress
        var imgStr = strImagePth + strImage
        let imgUrl = URL(string: imgStr)
        //        setImage.image = UIImage(named: imgStr)
        //        print(imgStr)
        
        let imgData = try? Data(contentsOf: imgUrl!)
        setImage.image = UIImage(data: imgData!)
        
//        setImage.image = UIImage(named: (strImagePth + strImage))
        
        stop()
    }
    
    func getData(strId: String, strPswd: String) {
        //        print("alamo")
        var chkId = ""
        var chkPswd = ""
        activ()
        Alamofire.request(getDataUrl).responseJSON { (responce) in
            //            print(responce)
            var validBool = false
            if(responce.result.value != nil){
                let resData = responce.result.value as! NSDictionary
                if let reqData : NSArray = resData["user"] as! NSArray{
                    //                print(reqData)
                    for loopVal in  reqData{
                        let jsonData = loopVal as! NSDictionary
                        chkId = jsonData.value(forKey: "log_id") as! String
                        chkPswd = jsonData.value(forKey: "user_email") as! String
                        if(strId == chkId && strPswd == chkPswd )
                        {
                            self.strName = jsonData.value(forKey: "user_name") as! String
                            self.strLogid = jsonData.value(forKey: "log_id") as! String
                            self.strEmail = jsonData.value(forKey: "user_email") as! String
                            self.strAddress = jsonData.value(forKey: "user_address") as! String
                            self.strImage = jsonData.value(forKey: "user_img") as! String
                            self.strContact = jsonData.value(forKey: "user_contact") as! String
                        }
                        
                    }
                }
            }
            else{
                self.getData(strId: self.strLogid, strPswd: self.strEmail)
            }
            self.stop()
            self.setValues()
        }
    }
    
    
    @IBAction func actionFillAttendance(_ sender: Any) {
        appDel.getDataCntr += 1
        let Yes = UNNotificationAction(identifier: "Yes", title: "Yes", options: .foreground)
        let No = UNNotificationAction(identifier: "NO", title: "No", options: .foreground)
        
        let cate = UNNotificationCategory(identifier: "SimpleNotification", actions: [Yes,No], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "", options: [])
        UNUserNotificationCenter.current().setNotificationCategories([cate])
        
        //        self.showNotification()
        activ()
        Alamofire.request(genarateOTPUrl).responseJSON { (responce) in
            //            print(responce.result.value)
            self.setOtp = responce.result.value as! String
            //            self.alertSetOtp(strOpt: self.setOtp)
            self.showNotification()
        }
    }
    
    @IBAction func actionLogout(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "logChk")
        resetDefaults()
        let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logView")
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    func alertSetOtp(strOpt:String) {
        //        showNotification()
        //set text field and get data from them
        var dateNtime = getDate()
        var splitDataNTime = dateNtime.split(separator: ".")
        var altDate = String(splitDataNTime[0])
        var altTime = String(splitDataNTime[1])
        //        var getdate = altDate.split(separator: "-")
        //        sysDate = Int(getdate[0])!
        let alertController =   UIAlertController(title: "OTP", message: "Date:"+altDate+"\n\nTime:"+altTime, preferredStyle: .alert)
        //Step:2
        let action = UIAlertAction(title: "Next", style: .default) { (alertAction) in
            let textFieldId = alertController.textFields![0] as UITextField
            var otp = textFieldId.text as! String
            //            print(otp)
            
            
//            self.appDel.btncntr += 1
//            print(self.appDel.btncntr)
            self.updateOtp()
            //self.performSegue(withIdentifier: "fillAttedance", sender: self)
        }
        
        //Step:3
        let clrAction = UIAlertAction(title: "Clear", style: .destructive, handler: nil)
        alertController.addTextField { (textField) in
            textField.isUserInteractionEnabled = false
            textField.text = strOpt
            //            print(strOpt)
        }
        //Step:4
        alertController.addAction(action)
        alertController.addAction(clrAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func updateOtp() {
        let param = [
            "logid": UserDefaults.standard.string(forKey: "logid"),
            "deviceID": appDel.dID,
            "otp": self.setOtp
        ]
        //        print(param.values)
        activ()
        Alamofire.upload(multipartFormData: { (multiPartFormData) in
            for (key, value) in param{
                multiPartFormData.append(value!.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: upadteOTPUrl ) { (res) in
            switch res{
            case .success(let upload, _, _):
                upload.uploadProgress(closure:{ (progress) in
                    //                    print("Upload Progress \(progress.fractionCompleted)")
                })
                upload.responseString(completionHandler: { (responce) in
                    print(responce)
                    
                    if let data = responce.value!.data(using: .utf8) {
                        do {
                            let nsdData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                            let nsdArr = nsdData as! NSDictionary
                            //print(nsdArr.value(forKey: "Status"))
                            if(nsdArr.value(forKey: "Status") as! String == "ok"){
                                self.performSegue(withIdentifier: "fillAttedance", sender: self)
                            }
                            else{
                                let alert = UIAlertController(title: "Stop Cheating", message: "Device Id Not Matched", preferredStyle: .alert)
                                let defAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                alert.addAction(defAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                    self.stop()
                    //                    self.performSegue(withIdentifier: "fillAttedance", sender: self)
                })
            case .failure(_): break
            print(EncodingError.self)
            }
        }
    }
    
    func getDate() -> String {
        let date = NSDate()
        let dateFormater = DateFormatter()
        dateFormater.timeStyle = .short
        let dateStr = dateFormater.string(from: date as Date)
        dateFormater.dateFormat = "dd-MM-yy"
        let defaultTimeZone = dateFormater.string(from: date as Date)
        dateFormater.timeZone = NSTimeZone(abbreviation: "UTC") as! TimeZone
        let utcTimeZoneStr = dateFormater.string(from: date as Date)
        
        var strTime = ("\(utcTimeZoneStr).\(dateStr)")
        
        
        return strTime
    }
    
    func showNotification() {
        let content = UNMutableNotificationContent()
        
        content.title = "Attendance OTP"
        //        content.subtitle = "Notification sub title"
        content.body = "You otp for attendance is \(self.setOtp)"
        //        content.badge = 0
        content.categoryIdentifier = "SimpleNotification"
        
        let tigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5, repeats: false)
        
        let req = UNNotificationRequest(identifier: "any", content: content, trigger: tigger)
        
        //UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().add(req, withCompletionHandler: nil)
        //        NSObject.perform(#selector(callFunc), with: nil, afterDelay: 1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            // your code here
            self.stop()
            self.alertSetOtp(strOpt: self.setOtp)
        }
    }
    //    @objc func sayHello()
    //    {
    //        NSLog("hello World")
    //    }
    
    func activ(){
        DispatchQueue.main.async {
            self.activityProgress.startAnimating()
            self.activityProgress.isHidden = false
        }
    }
    
    func stop() {
        DispatchQueue.main.async {
            self.activityProgress.stopAnimating()
            //                                self.tblView.isHidden = false
            self.activityProgress.isHidden = true
            //                                self.tblView.reloadData()
        }
    }
    
    //    func keyboardWillShowForResizing(notification: Notification) {
    //        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
    //            let window = self.view.window?.frame {
    //            // We're not just minusing the kb height from the view height because
    //            // the view could already have been resized for the keyboard before
    //            self.view.frame = CGRect(x: self.view.frame.origin.x,
    //                                     y: self.view.frame.origin.y,
    //                                     width: self.view.frame.width,
    //                                     height: window.origin.y + window.height - keyboardSize.height)
    //        } else {
    //            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
    //        }
    //    }
    
    
    
    
    //    @objc func callFunc(){
    //        alertSetOtp(strOpt: setOtp)
    //    }
    
}
