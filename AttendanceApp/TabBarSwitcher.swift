//
//  TabBarSwitcher.swift
//  AttendanceApp
//
//  Created by Nan on 04/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class TabBarSwitcher: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    //        initSwipe(direction: L)
        // Do any additional setup after loading the view.
    }
    func initSwipe(direction:UISwipeGestureRecognizer){
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(sender:)))
//        swipe.direction = direction
        swipe.direction = .left
        self.view.addGestureRecognizer(swipe)
    }
    @objc func handleSwipes(sender:UISwipeGestureRecognizer){
        if (sender.direction == .left) {
            tabBarController?.selectedIndex = 1
        }
        
    }

}



