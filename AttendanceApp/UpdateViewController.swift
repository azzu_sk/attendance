//
//  UpdateViewController.swift
//  AttendanceApp
//
//  Created by Nan on 31/12/18.
//  Copyright © 2018 Nan. All rights reserved.
//

import UIKit
import AssetsLibrary
import Alamofire

@available(iOS 11.0, *)
class UpdateViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var upImg: UIImageView!
    @IBOutlet weak var upName: UITextField!
    @IBOutlet weak var upEmail: UITextField!
    @IBOutlet weak var upContact: UITextField!
    @IBOutlet weak var upAddress: UITextField!
    @IBOutlet weak var editBtn: UIButton!
    
    var strName = ""
    var strContact = ""
    var strEmail = ""
    var strAddress = ""
    var appDel:AppDelegate!
//        var strImagePth = "‎⁨/Users/nan/.bitnami/stackman/machines/xampp/volumes/root/htdocs/attendance/uploads/"
//        var urlUpdate = "http://localhost:8080/attendance/updateData.php"
    var strImagePth = "https://testattendancerox.000webhostapp.com/attendance/uploads/"
    var urlUpdate = "https://testattendancerox.000webhostapp.com/attendance/updateData.php"
    var strImage = ""
    var imgFileName = ""
    var imgFileData : Data!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        appDel = UIApplication.shared.delegate as! AppDelegate
        
        editBtn.layer.cornerRadius = 10
        editBtn.clipsToBounds = true
        
        upName.text = strName
        upEmail.text = strEmail
        upContact.text = strContact
        upAddress.text = strAddress
//        upImg.image = UIImage(named: strImagePth + strImage)
        var imgStr = strImagePth + strImage
        let imgUrl = URL(string: imgStr)
        if(imgUrl == nil){
            //            upImg.image = UIImage(named: "404")
            print("Image \(imgStr)")
            upImg.image = UIImage(named: imgStr)
        }else{
            let imgData = try? Data(contentsOf: imgUrl!)
            upImg.image = UIImage(data: imgData!)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 150
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func actionEditImg(_ sender: Any) {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self
        myPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPicController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let imgToData = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        let imgToSet = imgToData.cgImage
        //        upImg.image = UIImage(cgImage: imgToSet!)
        if let imgToSaveData = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL{
            ALAssetsLibrary().asset(for: imgToSaveData as URL, resultBlock: { (assets) in
                let fileName = assets?.defaultRepresentation()?.filename()
                self.imgFileName = fileName!
                //                print("Image name is \(self.imgFileName)")
            }, failureBlock: nil)
        }
        self.imgFileData = imgToData.jpegData(compressionQuality: 0.2)
        //        print(("Image Data is \(imgFileData)"))
        upImg.image = UIImage(cgImage: imgToSet!)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateData() {
        //        print("UpdateAction")
//        UserDefaults.standard.string(forKey: "name")
        UserDefaults.standard.set(upName.text, forKey: "name")
        strName = upName.text!
        strEmail = upEmail.text!
        strContact = upContact.text!
        strAddress = upAddress.text!
        
        if(imgFileData == nil && imgFileName == "")
        {
            imgFileData = upImg.image?.jpegData(compressionQuality: 0.2)
            imgFileName = strImage
        }
        let param = [
            "logid":UserDefaults.standard.string(forKey: "logid"),
            "name":strName,
            "email":strEmail,
            "contact":strContact,
            "address":strAddress
        ]
        
        Alamofire.upload(multipartFormData: { (multiPartFormData) in
            multiPartFormData.append(self.imgFileData, withName: "fileToUpload", fileName: self.imgFileName, mimeType: "image/png")
            for (key, value) in param{
                multiPartFormData.append(value!.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: urlUpdate) { (res) in
            switch res{
            case .success(let upload, _, _):
                upload.uploadProgress(closure:{ (progress) in
                    //                    print("Upload Progress \(progress.fractionCompleted)")
                })
                upload.responseString(completionHandler: { (responce) in
                    //                    print(responce)
                })
                //self.objProfile.getData(strId: self.appDel.log_id, strPswd: self.strEmail)
                //                self.navigationController?.popViewController(animated: true)
                let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController")
                self.present(navigationController, animated: true, completion: nil)
            case .failure(_): break
                //                print(EncodingError.self)
            }
        }
    }
    @IBAction func actionUpdateData(_ sender: Any) {
        appDel.getDataCntr += 1
        updateData()
        //        self.navigationController?.popViewController(animated: true)
    }
}

