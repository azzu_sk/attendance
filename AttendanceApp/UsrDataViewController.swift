//
//  UsrDataViewController.swift
//  AttendanceApp
//
//  Created by Nan on 03/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Alamofire
@available(iOS 11.0, *)
class UsrDataViewController: UIViewController {
    

//    @IBOutlet weak var lblLogid:UILabel!
//    @IBOutlet weak var lblName:UILabel!
//    @IBOutlet weak var lblDate:UILabel!
//    @IBOutlet weak var lblTime:UILabel!
    
    @IBOutlet weak var filterPage: UIView!
    var cntrFilter = true
    @IBOutlet weak var tblview: UITableView!
    var chkDate = ""
    var strDate = ""
    var strTime = ""
    var strOtp = ""
    var sendChkOut = ""
    var nsdArr:NSDictionary!
    var logID = UserDefaults.standard.string(forKey: "logid")
    var dbDate:NSArray = []
    var dbIn:NSArray = []
    var dbOut:NSArray = []
    var emtArr:NSArray = []
    var cntr = 0
    
    var insertUrl = "https://testattendancerox.000webhostapp.com/attendance/insertData.php"
    var getAttendanceUrl = "https://testattendancerox.000webhostapp.com/attendance/getAttendance.php"

    var appDel:AppDelegate!
//    var insertUrl = "http://localhost:8080/attendance/insertData.php"
//    var getAttendanceUrl = "http://localhost:8080/attendance/getAttendance.php"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        var setDate = getDate()
        var strSplit = setDate.split(separator: ".")
        strDate = String(strSplit[0])
        strTime = String(strSplit[1])
//        lblLogid.text = lblLogid.text! + UserDefaults.standard.string(forKey: "logid")!
//        lblName.text = lblName.text! + UserDefaults.standard.string(forKey: "name")!
//        lblDate.text = lblDate.text! + strDate
//        lblTime.text = lblTime.text! + strTime
        
//        getAttendance()
        appDel = UIApplication.shared.delegate as! AppDelegate
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        inertData()
    }
    
    func getDate() -> String {
        let date = NSDate()
        let dateFormater = DateFormatter()
        dateFormater.timeStyle = .short
        let dateStr = dateFormater.string(from: date as Date)
        dateFormater.dateFormat = "dd-MM-yy"
        let defaultTimeZone = dateFormater.string(from: date as Date)
        dateFormater.timeZone = NSTimeZone(abbreviation: "UTC") as! TimeZone
        let utcTimeZoneStr = dateFormater.string(from: date as Date)
        
        var strTime = ("\(utcTimeZoneStr).\(dateStr)")
        
//        print(strTime)
        return strTime
    }
    func inertData() {
        var outChk = ""
        let param = [
            "logid":UserDefaults.standard.string(forKey: "logid"),
            "strDate":strDate,
            "strTime":strTime,
            "otp":strOtp
        ]
        let chkOut = sendChkOut
        
        Alamofire.upload(multipartFormData: { (multiPartFormData) in
            multiPartFormData.append(chkOut.data(using: String.Encoding.utf8)!, withName: "out")
            for (key, value) in param{
                multiPartFormData.append(value!.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: insertUrl) { (res) in
            switch res{
            case .success(let upload, _, _):
                upload.uploadProgress(closure:{ (progress) in
                    //                    print("Upload Progress \(progress.fractionCompleted)")
                })
                upload.responseString(completionHandler: { (responce) in
                                        print(responce.value)
                    var str = responce.value as! NSString
                    if let jsonString = str as? String {
                        let objectData = jsonString.data(using: String.Encoding.utf8)
                        do {
                            let json = try JSONSerialization.jsonObject(with: objectData!, options: JSONSerialization.ReadingOptions.mutableContainers)
                            print(String(describing: json))
                            let new  =  json as! NSDictionary
                            print(new.value(forKey: "Status"))
                            outChk = new.value(forKey: "Status") as! String
                            
                        } catch {
                            // Handle error
                            print(error)
                        }
                    }
                    print(outChk)
                    if(outChk == "outOk"){
                        self.appDel.btncntr = "in"
                        print("out ok")
                    }
                    if(outChk == "InOk"){
                        self.appDel.btncntr = "out"
                        print("in ok")
                    }
                    self.getAttendance()
                })
                
            case .failure(_):
                                print(EncodingError.self)
                break
            }
        }
    }
    @IBAction func actionHome(_ sender: Any) {
//        let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController")
//        self.present(navigationController, animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionFilter(_ sender: Any) {
        if(cntrFilter){
            filterPage.isHidden = false
            cntrFilter = false
        }
        else{
            filterPage.isHidden = true
            cntrFilter = true
        }
    }
    
}
@available(iOS 11.0, *)
extension UsrDataViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.dbDate.count >= 1){
            cntr = 0
            return self.dbDate.count
        }
        else{
            cntr = 0
            return 0
        }
        //return self.dbDate.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if(cntr == 0){
//            appDel.btnDate = dbDate[indexPath.row] as! String
////            print(appDel.btnDate)
//            cntr += 1
//        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceTableViewCell") as! AttendanceTableViewCell
        cell.configureCell(strDate: "\(dbDate[indexPath.row])", strInTime: " \(dbIn[indexPath.row])", strOutTime:" \(dbOut[indexPath.row])")
//        cell.layer.cornerRadius = 80
//        cell.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.layer.borderWidth = 5
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.3, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
        },completion: { finished in
            UIView.animate(withDuration: 0.1, animations: {
                cell.layer.transform = CATransform3DMakeScale(1,1,1)
            })
        })
    }
 
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblview.reloadData()
    }
    
    func getAttendance() {
        Alamofire.upload(multipartFormData: { (multi) in
            multi.append((self.logID?.data(using: String.Encoding.utf8))!, withName: "logid")
        }, to: getAttendanceUrl) { (res) in
            switch res{
            case .success(let upload, _, _):
                upload.uploadProgress(closure:{ (progress) in
                    //                    print("Upload Progress \(progress.fractionCompleted)")
                })
                upload.responseString(completionHandler: { (responce) in
//                    print(responce)
                    if let data = responce.value!.data(using: .utf8) {
                        do {
                            let nsdData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                            self.nsdArr = nsdData as! NSDictionary
                            let dbData = self.nsdArr["user"] as! NSArray
                            self.dbDate = dbData.value(forKey: "date") as! NSArray
                            self.dbIn = dbData.value(forKey: "in_time") as! NSArray
                            self.dbOut = dbData.value(forKey: "out_time") as! NSArray
//                            print(self.dbDate)
//                            print(self.dbIn)
//                            print(self.dbOut)
                            self.tblview.reloadData()
                            
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                })
                break
            case .failure(_):
                print(EncodingError.self)
            }
        }
    }
    
    
}
