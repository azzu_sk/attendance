//
//  ViewController.swift
//  AttendanceApp
//
//  Created by Nan on 28/12/18.
//  Copyright © 2018 Nan. All rights reserved.
//

import UIKit
import QuartzCore
import Alamofire

@available(iOS 11.0, *)
class ViewController: UIViewController {
    
    @IBOutlet weak var txtUserId: UITextField!
    @IBOutlet weak var txtUserPassword: UITextField!
    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var setImg: UIImageView!
    
    var appDel:AppDelegate!
    
    let url = "https://testattendancerox.000webhostapp.com/attendance/getData.php"
//    let url = "http://localhost:8080/attendance/getData.php"
    var usrId = ""
    var usrPswd = ""
    
    var strName = ""
    var strLogid = ""
    var strEmail = ""
    var strAddress = ""
    var strImage = ""
    var strContact = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        ConvToStr.init()
        
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        
        btnLogIn.layer.cornerRadius = 15
        btnLogIn.clipsToBounds = true
    }
    
    @IBAction func actionLogIn(_ sender: Any) {
        alertActionUserId()
        alertActionUserPswd()
    }
//    func getdata(){
//        print(url)
//        Alamofire.request(url).responseJSON{ (response) in
//            print(response.result.value)
//        }
//        }
    
    
    func getData(strId: String, strPswd: String) {
//        print("alamo")
        var chkId = ""
        var chkPswd = ""
        Alamofire.request(url).responseJSON { (responce) in
//            print(responce)
            var validBool = false
            let data = responce.result.value
//            print(data)
            
            let resData = data as! NSDictionary
            
            if let reqData : NSArray = (resData["user"] as! NSArray){
//                print(reqData)
                
                for loopVal in  reqData{
                    let jsonData = loopVal as! NSDictionary
                    
//                    print(jsonData.value(forKey: "p_id"))
                    chkId = jsonData.value(forKey: "log_id") as! String
                    chkPswd = jsonData.value(forKey: "user_email") as! String
                    //var imgset = jsonData.value(forKey: "user_img") as! String
                    //self.setImg.image = UIImage(named: "/Users/nan/.bitnami/stackman/machines/xampp/volumes/root/htdocs/attendance/uploads/" + imgset)
//                    print(strId)
//                    print(strPswd)
//                    print(chkId)
//                    print(chkPswd)
                    if(strId == chkId && strPswd == chkPswd){
                        //return true
                        
                        self.strName = jsonData.value(forKey: "user_name") as! String
                        self.strLogid = jsonData.value(forKey: "log_id") as! String
                        self.strEmail = jsonData.value(forKey: "user_email") as! String
                        self.strAddress = jsonData.value(forKey: "user_address") as! String
                        self.strImage = jsonData.value(forKey: "user_img") as! String
                        self.strContact = jsonData.value(forKey: "user_contact") as! String
                        UserDefaults.standard.set(true, forKey: "logChk")
                        UserDefaults.standard.set(self.strLogid, forKey: "logid")
                        UserDefaults.standard.set(self.strEmail, forKey: "email")
                        UserDefaults.standard.set(self.strName, forKey: "name")
//                        print(self.strName)
                        validBool = true
                        
//                        self.alertAction(strTitle: "Logged In", strMsg: "Welcome")
                        self.appDel.promt = 1
                        self.redirectToProfile()
                    }
                    
                }
            }
            if(validBool != true){
                self.alertAction(strTitle: "Invalid", strMsg: "Insert correct Id and Password")
            }
        }
    }
    func alertAction(strTitle: String, strMsg: String) {
        let alert = UIAlertController(title: strTitle, message: strMsg, preferredStyle: .alert)
        let defAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(defAction)
        self.present(alert, animated: true, completion: nil)
    }
    func alertActionUserId() {
        //set text field and get data from them
        let alertController =   UIAlertController(title: "Log In", message: "Insert Id", preferredStyle: .alert)
        //Step:2
        let action = UIAlertAction(title: "Next", style: .default) { (alertAction) in
            let textFieldId = alertController.textFields![0] as UITextField
            self.usrId = textFieldId.text as! String
//            print(self.usrId)
            self.alertActionUserPswd()
        
        }
        //Step:3
        let clrAction = UIAlertAction(title: "Clear", style: .destructive, handler: nil)
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter Id"
        }
        //Step:4
        alertController.addAction(action)
        alertController.addAction(clrAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func alertActionUserPswd() {
        //set text field and get data from them
        let alertController =   UIAlertController(title: "Log In", message: "Insert Password", preferredStyle: .alert)
        //Step:2
        let action = UIAlertAction(title: "LogIn", style: .default) { (alertAction) in
            let textFieldId = alertController.textFields![0] as UITextField
            self.usrPswd = textFieldId.text as! String
//            print(self.usrPswd)
            self.getData(strId: self.usrId, strPswd: self.usrPswd)
        }
        //Step:3
        let clrAction = UIAlertAction(title: "Clear", style: .destructive, handler: nil)
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter Password"
        }
        //Step:4
        alertController.addAction(action)
        alertController.addAction(clrAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if(segue.identifier == "ProfileViewController"){
//            let dest = segue.destination as! ProfileViewController
//
//            dest.strName = self.strName
//            dest.strLogid = self.strLogid
//            dest.strEmail = self.strEmail
//            dest.strAddress = self.strAddress
//            dest.strImage = self.strImage
//            dest.strContact = self.strContact
//        }
//    }
    
    func redirectToProfile() {
        appDel.getDataCntr = 0
        let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController")
        self.present(navigationController, animated: true, completion: nil)
    }

}

