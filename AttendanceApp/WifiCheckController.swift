//
//  WifiCheckController.swift
//  AttendanceApp
//
//  Created by Nan on 02/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration.CaptiveNetwork
import CoreLocation

class WifiCheckController: UIViewController, CLLocationManagerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    override func viewDidAppear(_ animated: Bool) {
//        determineMyCurrentLocation()
    }
   
    @IBAction func actionWifi(_ sender: Any) {
        var currentWifi = ""
        if let interface = CNCopySupportedInterfaces() {
            for index in 0..<CFArrayGetCount(interface){
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interface, index)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfacedata = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                
                if let interfaceData = unsafeInterfacedata as?[String: AnyObject]{
                    currentWifi = interfaceData["SSID"] as! String
                    let bssid = interfaceData["BSSID"] as! String
                    let ssidData = interfaceData["SSIDDATA"] as! String
                    debugPrint("ssid = \(currentWifi), bssid = \(bssid), ssiddata = \(ssidData)")
                    
                    
                }
            }
        }
    }
    
    
    @IBAction func actionCordinates(_ sender: Any) {
        determineMyCurrentLocation()
        
    }
    
    func determineMyCurrentLocation() {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if(CLLocationManager.locationServicesEnabled()){
            locationManager.startUpdatingLocation()
            //print(locationManager.desiredAccuracy)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
}
