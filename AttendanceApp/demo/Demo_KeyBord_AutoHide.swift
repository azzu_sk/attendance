//
//  Demo_KeyBord_AutoHide.swift
//  AttendanceApp
//
//  Created by Nan on 11/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class Demo_KeyBord_AutoHide: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var lbl1:UITextField!
    @IBOutlet weak var lbl2:UITextField!
    @IBOutlet weak var lbl3:UITextField!
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.view.becomeFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                //self.view.frame.origin.y -= keyboardSize.height
                self.view.frame.origin.y -= 5
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
}
