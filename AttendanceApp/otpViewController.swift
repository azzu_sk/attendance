//
//  otpViewController.swift
//  AttendanceApp
//
//  Created by Nan on 07/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import UserNotifications

//@available(iOS 12.0, *)
@available(iOS 11.0, *)
class otpViewController: UIViewController, UNUserNotificationCenterDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
//            
//        })
    }
    @IBAction func actionOtp(_ sender: Any) {
        let Yes = UNNotificationAction(identifier: "Yes", title: "Yes", options: .foreground)
        let No = UNNotificationAction(identifier: "NO", title: "No", options: .foreground)
        
        let cate = UNNotificationCategory(identifier: "SimpleNotification", actions: [Yes,No], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "", options: [])
        UNUserNotificationCenter.current().setNotificationCategories([cate])
        
        self.showNotification()
    }
    
    func showNotification() {
        let content = UNMutableNotificationContent()
        
        content.title = "Notification title"
        content.subtitle = "Notification sub title"
        content.body = "Notification message body"
        content.badge = 0
        content.categoryIdentifier = "SimpleNotification"
        
        let tigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        let req = UNNotificationRequest(identifier: "any", content: content, trigger: tigger)
        
        //UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().add(req, withCompletionHandler: nil)

    }
}
