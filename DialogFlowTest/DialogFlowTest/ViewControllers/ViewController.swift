//
//  ViewController.swift
//  DialogFlowTest
//
//  Created by Nan on 19/02/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import AVFoundation
import ApiAI

class ViewController: UIViewController {
    
    var arr = [String]()
    var req = ""
    
    let speechSynthesizer = AVSpeechSynthesizer()
    @IBOutlet weak var lblResponce: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtRequest: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.tblView.estimatedRowHeight = 80
//        self.tblView.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        let scrollPoint = CGPoint(x: 0, y: self.tblView.contentSize.height - self.tblView.frame.size.height)
//        self.tblView.setContentOffset(scrollPoint, animated: false)
    }
    
    @IBAction func actionQuery(_ sender: Any) {
        let request = ApiAI.shared().textRequest()
        
        if let text = self.txtRequest.text, text != "" {
            request?.query = text
        } else {
            return
        }
        
        request?.setMappedCompletionBlockSuccess({ (request, response) in
            let response = response as! AIResponse
            if let textResponse = response.result.fulfillment.speech {
                self.speechAndText(text: textResponse)
                print(textResponse)
                self.arr.append("res;\(textResponse)")
                self.tblView.reloadData()
            }
        }, failure: { (request, error) in
            print(error!)
        })
//        print(txtRequest.text)
        req = txtRequest.text as! String
        arr.append("req;\(req)")
//        print()
        ApiAI.shared().enqueue(request)
        txtRequest.text = ""
    }
    

    func speechAndText(text: String) {
        let speechUtterance = AVSpeechUtterance(string: text)
        speechSynthesizer.speak(speechUtterance)
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseInOut, animations: {
            self.lblResponce.text = text
        }, completion: nil)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewTableViewCell") as! ViewTableViewCell
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "ViewTableViewCell1") as! ViewTableViewCell1
       // cell.lblRequest.text = arr[indexPath.row]
        if(arr[indexPath.row].contains("req")){
            var textSplit = arr[indexPath.row].split(separator: ";")
            print(textSplit)
            cell.lblReq.text = String(textSplit[1])
            return cell
        }
        else{
            var textSplit = arr[indexPath.row].split(separator: ";")
            print(textSplit)
            cell1.lblRes.text = String(textSplit[1])
            return cell1
        }
        
//        cell.lblRequest.sizeToFit()
//        cell.lblRequest.layoutIfNeeded()
        
    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let str = arr[indexPath.row]
//        let size = str.size(with: UIFont(name: "Helvetica", size: 17), constrainedToSize: CGSize(width: 280, height: 999), lineBreakMode: NSLineBreakMode.byWordWrapping)
//        print("\(size?.height ?? 0.0)")
//        return (size?.height ?? 0.0) + 10
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtRequest.resignFirstResponder()
        return true
    }
    
    
}

