//
//  ContentTableViewCell.swift
//  T_PASS
//
//  Created by Nan on 08/03/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit

class ContentTableViewCell: UITableViewCell {
    
    
    //Outlets
    @IBOutlet weak var userHeadPic : UIImageView!
    @IBOutlet weak var userHeadName : UILabel!
    @IBOutlet weak var contentPic : UIImageView!
    @IBOutlet weak var likeUnlikeBtn : UIButton!
    @IBOutlet weak var commentBtn : UIButton!
    @IBOutlet weak var sendBtn : UIButton!
    @IBOutlet weak var bookUnbookBtn : UIButton!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var likesCountLbl : UILabel!
    @IBOutlet weak var contentDesc : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
