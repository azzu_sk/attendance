//
//  NormalCell.swift
//  demo
//
//  Created by Nan on 30/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import ExpandableCell

class NormalCell: UITableViewCell {
    static var Nid = "NormalCell"
}
class ExpandedCell: ExpandableCell {
    static var Eid = "ExpandedCell"
}

class ExtItem: UITableViewCell {
    static var Iid = "ExtItem"
    @IBOutlet weak var titleLabel:UILabel!
}
