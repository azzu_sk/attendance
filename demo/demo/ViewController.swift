//
//  ViewController.swift
//  demo
//
//  Created by Nan on 30/01/19.
//  Copyright © 2019 Nan. All rights reserved.
//

import UIKit
import ExpandableCell

class ViewController: UIViewController {
    @IBOutlet weak var tblView:ExpandableTableView!
    
    var param = [NormalCell.Nid, ExpandedCell.Eid,NormalCell.Nid, ExpandedCell.Eid,NormalCell.Nid, ExpandedCell.Eid]
    
    var cell: UITableViewCell {
        return tblView.dequeueReusableCell(withIdentifier: ExtItem.Iid)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.expandableDelegate = self
        tblView.animation = .automatic
        
        tblView.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: NormalCell.Nid)
        tblView.register(UINib(nibName: "ExpandedCell", bundle: nil), forCellReuseIdentifier: ExpandedCell.Eid)
        tblView.register(UINib(nibName: "ExtItem", bundle: nil), forCellReuseIdentifier: ExtItem.Iid)
    }
}
extension ViewController:ExpandableDelegate{
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: param[indexPath.row]) else { return UITableViewCell() }
        return cell
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int
    {
        return param.count
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 40.0
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]?
    {
        switch indexPath.row {
        case 1:
            let cell1 = tblView.dequeueReusableCell(withIdentifier: ExtItem.Iid) as! ExtItem
            cell1.titleLabel.text = "First Expanded Cell"
            let cell2 = tblView.dequeueReusableCell(withIdentifier: ExtItem.Iid) as! ExtItem
            cell2.titleLabel.text = "Second Expanded Cell"
            let cell3 = tblView.dequeueReusableCell(withIdentifier: ExtItem.Iid) as! ExtItem
            cell3.titleLabel.text = "Third Expanded Cell"
            return [cell1, cell2, cell3]
            
        case 2:
            return [cell, cell]
        case 3:
            return [cell,cell, cell]
        case 5:
            return [cell,cell, cell]
        default:
            break
        }
        return nil
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]?
    {
        return [44, 44, 44]
    }
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

